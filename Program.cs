﻿namespace as1;

public class Program
{
    public static void Main(string[] args)
    {
        bool isValid = false;
        int n = 0;
        while (!isValid)
        {
            Console.WriteLine("Nhap so phan tu cua mang:");
            string input = Console.ReadLine();
            isValid = int.TryParse(input, out n) && n > 0;
            if (!isValid)
            {
                Console.WriteLine("So phan tu khong hop le, vui long nhap lai");
            }
        }
        GiaoVien[] danhsach = new GiaoVien[n];
        for (int i = 0; i < n; i++)
        {
            string name = "";
            isValid = false;
            while (!isValid)
            {
                Console.WriteLine("Ho va ten giao vien {0}:", i + 1);
                string input = Console.ReadLine();
                if (!input.Any(char.IsDigit))
                {
                    isValid = true;
                    name = input;
                }
                if (!isValid)
                {
                    Console.WriteLine("Ten khong duoc chua ki tu so, vui long nhap lai");
                }
            }
            isValid = false;
            int birthdate = 0;
            while (!isValid)
            {
                Console.WriteLine("Nam sinh:");
                string input = Console.ReadLine();
                isValid = int.TryParse(input, out birthdate) && birthdate > 0;
                if (!isValid)
                {
                    Console.WriteLine("Nam sinh khong hop le, vui long nhap lai");
                }
            }
            double salary = 0;
            isValid = false;
            while (!isValid)
            {
                Console.WriteLine("Luong co ban:");
                string input = Console.ReadLine();
                isValid = double.TryParse(input, out salary) && salary > 0;
                if (!isValid)
                {
                    Console.WriteLine("Luong khong hop le, vui long nhap lai");
                }

            }
            double coefficients_salary = 0;
            isValid = false;
            while (!isValid)
            {
                Console.WriteLine("He so luong:");
                string input = Console.ReadLine();
                isValid = double.TryParse(input, out coefficients_salary) && coefficients_salary > 0;
                if (!isValid)
                {
                    Console.WriteLine("He so luong khong hop le, vui long nhap lai");
                }
            }
            GiaoVien teacher = new GiaoVien(name, birthdate, salary, coefficients_salary);
            danhsach[i] = teacher;
        }
        double min_of_salary = danhsach[0].TinhLuong();
        int position = 0;
        for (int i = 0; i < n; i++)
        {
            if (danhsach[i].TinhLuong() < min_of_salary)
            {
                min_of_salary = danhsach[i].TinhLuong();
                position = i;
            }
        }
        Console.WriteLine("Thong tin cua giao vien co luong thap nhat");
        danhsach[position].XuatThongTin();
        Console.Read();
    }
}
