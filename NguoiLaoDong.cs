﻿namespace as1;
using System;
public class NguoiLaoDong
{
    private string hoten = "";
    private int namsinh = 0;
    private double luongcoban = 0;
    public string HoTen
    {
        get { return hoten; }
        set { hoten = value; }
    }
    public int NamSinh
    {
        get { return namsinh; }
        set { namsinh = value; }
    }
    public double LuongCoBan
    {
        get { return luongcoban; }
        set { luongcoban = value; }
    }
    public NguoiLaoDong()
    {
       
    }
    public NguoiLaoDong(string hoten, int namsinh, double luongcoban)
    {
        this.hoten = hoten;
        this.namsinh = namsinh;
        this.luongcoban = luongcoban;
    }
    public void NhapThongTin(string hoten, int namsinh, double luongcoban)
    {
        this.hoten = hoten;
        this.namsinh = namsinh;
        this.luongcoban = luongcoban;
    }
    public virtual double TinhLuong()
    {
        return luongcoban;
    }
    public void XuatThongTin()
    {
        Console.WriteLine("Ho ten la: {0}, nam sinh: {1}, luong co ban:{2}", hoten, namsinh, luongcoban);
    }
}
