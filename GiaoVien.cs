﻿namespace as1;

public class GiaoVien : NguoiLaoDong
{
    private double hesoluong = 0;
    public double HeSoLuong
    {
        get { return hesoluong; }
        set { hesoluong = value; }
    }
    public GiaoVien()
    {

    }
    public GiaoVien(string hoten, int namsinh, double luongcoban, double hesoluong) : base(hoten, namsinh, luongcoban)
    {
        this.hesoluong = hesoluong;
    }
    public void NhapThongTin(double hesoluong)
    {
        this.hesoluong = hesoluong;
    }
    public override double TinhLuong()
    {
        return LuongCoBan * HeSoLuong * 1.25;
    }
    public void XuLy()
    {
        hesoluong = hesoluong + 0.6;
    }
}
